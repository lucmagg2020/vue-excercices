let Translator = Vue.component('my-translator', {
    template:   `<div component="my-translator">
                    <h1>Traductor <b v-if="propLanguage=='ES'">Castellano-Ingles</b>
                                  <b v-else>Ingles-Castellano</b>
                    </h1>        
                    <input type="text" :placeholder="placeholderWord" v-model="word" />
                    <input type="button" @click="Clear" value="Limpiar" />
                    <div v-for="match in AnyMatch">
                        <span v-if="propLanguage=='ES'">{{ match.ES }}->{{ match.EN }}</span>
                        <span v-else>{{ match.EN }}->{{ match.ES }}</span>
                    </div>
                </div>`,
    data(){
        return{
            placeholderWord: 'Introduzca palabra a traducir:',
            word:'',
            dictionary:[{'EN':'Hello', 'ES':'Hola'},{'EN':'Bye', 'ES':'Chau'}]
        }
    },
    props:{ 
        propLanguage: { type:String, default:'ES' } 
    },
    computed:{
        AnyMatch(){
            let match=false;
            let words=[];
            this.dictionary.map((w)=>{if(this.word != '' && 
                w[this.propLanguage].toLowerCase().includes(this.word.toLowerCase())
            ) words.push(w)})
            return words
        }
    },
    methods:{ Clear(){ this.word = '' } }


})